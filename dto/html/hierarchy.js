var hierarchy =
[
    [ "DirectTrajectoryOptimization::BaseClass::ConstraintsBase< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1BaseClass_1_1ConstraintsBase.html", null ],
    [ "DirectTrajectoryOptimization::BaseClass::CostFunctionBolza< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1BaseClass_1_1CostFunctionBolza.html", [
      [ "DirectTrajectoryOptimization::Costs::MinimumEnergyCostBolza< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumEnergyCostBolza.html", null ],
      [ "DirectTrajectoryOptimization::Costs::MinimumTimeCostBolza< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumTimeCostBolza.html", null ],
      [ "DirectTrajectoryOptimization::Costs::MinimumVelocityCostBolza< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumVelocityCostBolza.html", null ],
      [ "DirectTrajectoryOptimization::Costs::ReachGoalCostBolza< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1Costs_1_1ReachGoalCostBolza.html", null ],
      [ "DirectTrajectoryOptimization::Costs::ZeroCostBolza< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1Costs_1_1ZeroCostBolza.html", null ]
    ] ],
    [ "DirectTrajectoryOptimization::DirectTrajectoryOptimizationProblem< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DirectTrajectoryOptimizationProblem.html", null ],
    [ "DirectTrajectoryOptimization::DTOMethodInterface< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DTOMethodInterface.html", [
      [ "DirectTrajectoryOptimization::DTODirectTranscription< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DTODirectTranscription.html", null ],
      [ "DirectTrajectoryOptimization::DTOMultipleShooting< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DTOMultipleShooting.html", null ]
    ] ],
    [ "DirectTrajectoryOptimization::DTOSolverInterface< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DTOSolverInterface.html", [
      [ "DirectTrajectoryOptimization::DTOIpoptInterface< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DTOIpoptInterface.html", null ],
      [ "DirectTrajectoryOptimization::DTOSnoptInterface< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1DTOSnoptInterface.html", null ]
    ] ],
    [ "DirectTrajectoryOptimization::nonlinearprogram", "classDirectTrajectoryOptimization_1_1nonlinearprogram.html", null ],
    [ "DirectTrajectoryOptimization::BaseClass::NumDiffDerivativesBase", "classDirectTrajectoryOptimization_1_1BaseClass_1_1NumDiffDerivativesBase.html", [
      [ "DirectTrajectoryOptimization::BaseClass::GenericConstraintsBase", "classDirectTrajectoryOptimization_1_1BaseClass_1_1GenericConstraintsBase.html", [
        [ "DirectTrajectoryOptimization::ShootingConstraint< DIMENSIONS >", "classDirectTrajectoryOptimization_1_1ShootingConstraint.html", null ]
      ] ]
    ] ]
];