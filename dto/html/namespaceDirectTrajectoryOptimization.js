var namespaceDirectTrajectoryOptimization =
[
    [ "BaseClass", "namespaceDirectTrajectoryOptimization_1_1BaseClass.html", "namespaceDirectTrajectoryOptimization_1_1BaseClass" ],
    [ "Costs", null, [
      [ "MinimumEnergyCostBolza", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumEnergyCostBolza.html", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumEnergyCostBolza" ],
      [ "MinimumTimeCostBolza", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumTimeCostBolza.html", null ],
      [ "MinimumVelocityCostBolza", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumVelocityCostBolza.html", "classDirectTrajectoryOptimization_1_1Costs_1_1MinimumVelocityCostBolza" ],
      [ "ReachGoalCostBolza", "classDirectTrajectoryOptimization_1_1Costs_1_1ReachGoalCostBolza.html", "classDirectTrajectoryOptimization_1_1Costs_1_1ReachGoalCostBolza" ],
      [ "ZeroCostBolza", "classDirectTrajectoryOptimization_1_1Costs_1_1ZeroCostBolza.html", null ]
    ] ],
    [ "Methods", "namespaceDirectTrajectoryOptimization_1_1Methods.html", null ],
    [ "Solvers", "namespaceDirectTrajectoryOptimization_1_1Solvers.html", null ],
    [ "DirectTrajectoryOptimizationProblem", "classDirectTrajectoryOptimization_1_1DirectTrajectoryOptimizationProblem.html", "classDirectTrajectoryOptimization_1_1DirectTrajectoryOptimizationProblem" ],
    [ "DTOMethodInterface", "classDirectTrajectoryOptimization_1_1DTOMethodInterface.html", "classDirectTrajectoryOptimization_1_1DTOMethodInterface" ],
    [ "DTODirectTranscription", "classDirectTrajectoryOptimization_1_1DTODirectTranscription.html", "classDirectTrajectoryOptimization_1_1DTODirectTranscription" ],
    [ "DTOMultipleShooting", "classDirectTrajectoryOptimization_1_1DTOMultipleShooting.html", "classDirectTrajectoryOptimization_1_1DTOMultipleShooting" ],
    [ "ShootingConstraint", "classDirectTrajectoryOptimization_1_1ShootingConstraint.html", "classDirectTrajectoryOptimization_1_1ShootingConstraint" ],
    [ "nonlinearprogram", "classDirectTrajectoryOptimization_1_1nonlinearprogram.html", null ],
    [ "DTOSolverInterface", "classDirectTrajectoryOptimization_1_1DTOSolverInterface.html", "classDirectTrajectoryOptimization_1_1DTOSolverInterface" ],
    [ "DTOIpoptInterface", "classDirectTrajectoryOptimization_1_1DTOIpoptInterface.html", "classDirectTrajectoryOptimization_1_1DTOIpoptInterface" ],
    [ "DTOSnoptInterface", "classDirectTrajectoryOptimization_1_1DTOSnoptInterface.html", "classDirectTrajectoryOptimization_1_1DTOSnoptInterface" ]
];