var direct__trajectory__optimization__problem_8hpp =
[
    [ "DirectTrajectoryOptimizationProblem", "classDirectTrajectoryOptimization_1_1DirectTrajectoryOptimizationProblem.html", "classDirectTrajectoryOptimization_1_1DirectTrajectoryOptimizationProblem" ],
    [ "method_t", "direct__trajectory__optimization__problem_8hpp.html#a4444e6b6832cbf56cda4c1276a436e85", null ],
    [ "solver_t", "direct__trajectory__optimization__problem_8hpp.html#a16bb42a23080bde693cf16b57b8929c3", null ],
    [ "Method", "direct__trajectory__optimization__problem_8hpp.html#ac1c559055dd6eddd42bc232c6d4fac49", [
      [ "DIRECT_TRANSCRIPTION", "direct__trajectory__optimization__problem_8hpp.html#ac1c559055dd6eddd42bc232c6d4fac49a1e29e01b22923d6d3a5077ae422195a2", null ],
      [ "MULTIPLE_SHOOTING", "direct__trajectory__optimization__problem_8hpp.html#ac1c559055dd6eddd42bc232c6d4fac49af88d1e603b436aac16113d678668ea48", null ]
    ] ],
    [ "Solver", "direct__trajectory__optimization__problem_8hpp.html#af9b2b21511a81827c7467109b06ef189", [
      [ "SNOPT_SOLVER", "direct__trajectory__optimization__problem_8hpp.html#af9b2b21511a81827c7467109b06ef189abe25b412dcf85c9be3e8ac387c13aa59", null ],
      [ "IPOPT_SOLVER", "direct__trajectory__optimization__problem_8hpp.html#af9b2b21511a81827c7467109b06ef189ab5575a04ca9ec7c6d126ad74a273e726", null ]
    ] ]
];